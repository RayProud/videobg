var gulp = require("gulp");
var jade = require("gulp-jade");
var styl = require("gulp-stylus");
var connect = require("gulp-connect");

gulp.task('connect', function() {
  connect.server({
    root: '../Production/',
    livereload: true,
    port: 1337
  });
});

gulp.task('styl', function () {
    gulp.src('./styl/style.styl')
        .pipe(styl())
        .pipe(gulp.dest('../Production/css/'))
        .pipe(connect.reload());
});

gulp.task('jade', function () {
    gulp.src('./jade/*.jade')
        .pipe(jade({
          pretty: true
        }))
        .pipe(gulp.dest('../Production/'))
        .pipe(connect.reload());
});

gulp.task('watch', function() {
  gulp.watch('./styl/*.styl', ['styl']);
  gulp.watch('./jade/*.jade', ['jade']);
  gulp.watch('../Production/js/*.js', ['jade']);
});

gulp.task('default', ['styl', 'jade', 'connect', 'watch']);
