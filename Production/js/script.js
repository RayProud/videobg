$(function(){

  var $inputEmail = $('.email-input');
  var $inputName = $('.text-input');
  var $submitBtn = $('.submit-btn');
  var $errorEmail = $('.error-email');
  var $errorName = $('.error-name');
  var $errorMobile = $('.error-mobile');
  var $successBlock = $('.success-form');
  var $formWrap = $('.form-wrap');
  var $form = $('.form');
  var formAction = $('.form').attr('action');
  var formId = $('[name="FORM_ID"]').attr('value');

  $inputName.on('focus', function() {
    $errorName.slideUp();
    $errorMobile.slideUp();
  });

  $inputEmail.on('focus', function() {
    $errorEmail.slideUp();
    $errorMobile.slideUp();
  });



  $submitBtn.on('click', function(e){
    e.preventDefault();
    var emailInputVal = $inputEmail.val();
    var nameInputVal = $inputName.val();
    var validEmail = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    var emailTest = emailInputVal != '' && validEmail.test(emailInputVal);
    var nameTest = nameInputVal != '';

    if(emailTest) {
      $errorEmail.slideUp();
    } else {
      e.preventDefault();
      $errorEmail.slideDown();
      $errorMobile.slideDown();
    }

    if(nameTest) {
      $errorName.slideUp();
    } else {
      e.preventDefault();
      $errorName.slideDown();
      $errorMobile.slideDown();
    }

    if(emailTest && nameTest) {
      $formWrap.slideUp();
      $successBlock.slideDown();
      jQuery.ajax({
          url: formAction,
          type: "POST", 
          data: {
                  FIRST_NAME: nameInputVal,
                  EMAIL: emailInputVal,
                  FORM_ID: formId
                },
          dataType: "html"
        });
      e.preventDefault();
      return false;
    }
  });

  $('.content').click(function(){
    if($successBlock.is(':visible')) {
      $inputEmail.val('');
      $inputName.val('');

      $successBlock.slideUp();
      $formWrap.slideDown();
    }
  });

});